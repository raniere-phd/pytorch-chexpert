# PyTorch CheXpert Fracture

This repository is a Machine Learning model implemented in PyTorch
to detect fracture in the [CheXpert data](https://stanfordmlgroup.github.io/competitions/chexpert/).

## Setting Environment with Conda

Run

```
$ conda env create --file environment.txt
```

## Data

1. Visit https://stanfordmlgroup.github.io/competitions/chexpert/.
2. Fill the *Downloading the Dataset (v1.0)* form.
3. Download the data and unzip it.
4. Run `ln -s /path/to/CheXpert/data/ data && ls`.
   You should have `CheXpert-v1.0-small` in the output.
5. Run `python data-sample.py`.

## Machine Learning Experiment

Run

```
$ python main.py
```

Metrics will be save in [Comet](https://www.comet.ml/).

## Files and Folders

- `agent.py`

  Has the knowledge to handle the data and model for train, validation and test.
- `data`

  Store the data. It should be replace with a symbolic link to the data.
- `dataset.py`

  Has the knowledge to load the data and create batches.
- `environment.yml`

  Conda environment.
- `main.py`

  Runnable script. It stores some hyperparameters.
- `model.py`

  Has the implementation of the model using PyTorch.
- `weights`

  Store the weights of trained model.

## References

- Irvin, Jeremy, et al. CheXpert: A Large Chest Radiograph Dataset with Uncertainty Labels and Expert Comparison. 21 Jan. 2019. arXiv.org, http://arxiv.org/abs/1901.07031.