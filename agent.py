"""
Agent

Includes all the functions that train, validate, and test the model.
"""
import logging

from sklearn import metrics

import torch
from torch import nn
import torch.optim as optim

import dataset


logger = logging.getLogger("pytorch.agent")


class Agent:
    """
    This base class will contain the base functions to be overloaded by any agent you will implement.
    """

    def __init__(self, hyper_params, comet_logger=None):
        """Create instance of Agent"""
        self.hyper_params = hyper_params
        self.comet = comet_logger
        logger.debug("Creating model ...")
        self.model = torch.hub.load(
            "pytorch/vision:v0.9.0",
            self.hyper_params["model"],
            pretrained=False,
            num_classes=2,
        )
        logger.debug("Creating model completed.")
        logger.debug("Sending model to device ...")
        self.model.to(self.hyper_params["device"])
        logger.debug("Sending model to device completed.")

        self.data_train_loader = None
        self.data_test_loader = None

    def train(self):
        """
        Main training loop
        :return:
        """
        if self.data_train_loader is None:
            logger.debug("Creating data train loader ...")
            [self.data_train_loader, self.data_test_loader] = dataset.kfold(
                self.hyper_params["batch_size"]
            )
            logger.debug("Creating data train loader completed.")

        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.SGD(self.model.parameters(), lr=0.001, momentum=0.9)

        logger.info("Training model ...")
        for epoch in range(self.hyper_params["num_epochs"]):
            logger.info("Running epoch #{} ...".format(epoch))
            self.comet.set_epoch(epoch)

            self.model.train()
            epoch_loss = self.train_one_epoch() / len(self.data_train_loader)
            self.comet.log_metric("epoch_loss", epoch_loss, epoch)
            logger.info(
                "Running epoch #{} completed with {} running loss.".format(
                    epoch, epoch_loss
                )
            )

    def train_one_epoch(self):
        """
        One epoch of training
        :return:
        """
        running_loss = 0

        for batch_ndx, sample in enumerate(self.data_train_loader):
            logger.debug("Processing batch #{} ...".format(batch_ndx))
            self.comet.set_step(batch_ndx)

            inputs, labels = sample.values()
            inputs = inputs.to(self.hyper_params["device"], dtype=torch.float)
            labels = labels.to(self.hyper_params["device"], dtype=torch.long)

            # zero the parameter gradients
            self.optimizer.zero_grad()

            # forward + backward + optimize
            outputs = self.model(inputs)
            loss = self.criterion(outputs, labels)
            loss.backward()
            self.optimizer.step()

            running_loss += float(loss.item())

            logger.debug(
                "Running batch #{} completed with {} running loss.".format(
                    batch_ndx, running_loss
                )
            )

        return running_loss

    def validate(self):
        """
        Main validate loop
        :return:
        """
        pass

    def test(self):
        """
        Main test loop
        :return:
        """
        if self.data_test_loader is None:
            logger.debug("Creating data test loader ...")
            [self.data_train_loader, self.data_test_loader] = dataset.kfold(
                self.hyper_params["batch_size"]
            )
            logger.debug("Created data test loader.")

        logger.info("Testing model ...")

        all_labels = []
        all_preds = []

        with torch.no_grad():
            for batch_ndx, sample in enumerate(self.data_test_loader):
                logger.debug("Processing batch #{} ...".format(batch_ndx))
                inputs, labels = sample.values()
                inputs = inputs.to(self.hyper_params["device"], dtype=torch.float)
                labels = labels.to(self.hyper_params["device"], dtype=torch.long)
                all_labels.extend(labels.detach().tolist())

                self.model.eval()
                outputs = self.model(inputs)
                outputs = torch.nn.functional.softmax(outputs, dim=1)
                _, preds = torch.max(outputs, 1)
                all_preds.extend(preds.detach().tolist())

        self.comet.log_metrics(
            {
                "Accuracy": metrics.accuracy_score(all_labels, all_preds),
                "Precision": metrics.precision_score(all_labels, all_preds),
                "F1 Score": metrics.f1_score(all_labels, all_preds),
            }
        )

        self.comet.log_confusion_matrix(y_true=all_labels, y_predicted=all_preds)

    def finalize(self):
        """
        Finalizes all the operations
        :return:
        """
        pass
