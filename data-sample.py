"""
Data Sample

Create CSV file from CheXpert with 1000 fracture and 1000 no-fracture cases.
"""

import pandas as pd

DATA_DIR = "data/CheXpert-v1.0-small/"

df = pd.read_csv(DATA_DIR + "train.csv")

# blank for unmentioned
#     0 for negative
#    -1 for uncertain
#     1 for positive
df_fracture = df.loc[
    (df["Fracture"].isin([0, 1])) & (df["Frontal/Lateral"] == "Frontal")
]

df_is_fracture_sample = df_fracture.loc[df_fracture["Fracture"] == 1].sample(n=1000)
df_is_not_fracture_sample = df_fracture.loc[df_fracture["Fracture"] == 0].sample(n=1000)

df_is_fracture_sample.to_csv(
    DATA_DIR + "fracture_1000.csv",
    columns=["Path", "Sex", "Age", "Fracture"],
    index=False,
)
df_is_not_fracture_sample.to_csv(
    DATA_DIR + "fracture_not_1000.csv",
    columns=["Path", "Sex", "Age", "Fracture"],
    index=False,
)
