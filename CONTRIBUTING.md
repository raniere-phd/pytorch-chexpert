# Config Git Pre-Commit Hook

```
$ cp pre-commit ./git/hooks
```

# Format `*.py` Files

```
$ python -m black .
```

# Check `*.py` for Errors

```
$ python -m pyflakes .
```

# Update Environment

```
$ conda list --explicit > environment.txt
```