"""
Dataset

Includes all the functions that either preprocess or postprocess data.
"""
import os
import os.path

import pandas as pd
import torch.utils.data
from torchvision import transforms
from torchvision.io import read_image
from torchvision.io.image import ImageReadMode


class Dataset(torch.utils.data.Dataset):
    """Dataset"""

    def __init__(self, is_fracture=True):
        """Create instance of dataset"""
        if is_fracture:
            self.df = pd.read_csv("data/CheXpert-v1.0-small/fracture_1000.csv")
        else:
            self.df = pd.read_csv("data/CheXpert-v1.0-small/fracture_not_1000.csv")

    def __len__(self):
        return self.df.shape[0]

    def __getitem__(self, idx):
        img_path = os.path.join("data", self.df.loc[idx, "Path"])
        image = read_image(img_path, ImageReadMode.RGB)

        preprocess = transforms.Compose(
            [
                transforms.Resize(320),
                transforms.CenterCrop(320),
            ]
        )
        image = preprocess(image)

        label = self.df.loc[idx, "Fracture"]

        return {"image": image, "label": label}

    def plot_samples_per_epoch(self):
        """
        Plotting the batch images
        :return: img_epoch: which will contain the image of this epoch
        """
        raise NotImplementedError

    def make_gif(self):
        """
        Make a gif from a multiple images of epochs
        :return:
        """
        raise NotImplementedError

    def finalize(self):
        pass


class DataLoader(torch.utils.data.DataLoader):
    """MNIST data loader"""

    def __init__(self, dataset, batch_size, shuffle=True):
        """Create data loader instance."""
        super().__init__(dataset, batch_size, shuffle=shuffle)


def kfold(batch_size, n_splits=5):
    """K-Folds cross-validator"""
    dataset_is_fracture = Dataset(is_fracture=True)
    dataset_is_not_fracture = Dataset(is_fracture=False)

    fold_size = int(
        len(dataset_is_fracture) / n_splits
    )  # need type conversion as randperm() requires int

    [train_is_fracture, test_is_fracture] = torch.utils.data.random_split(
        dataset_is_fracture, ((n_splits - 1) * fold_size, fold_size)
    )
    [train_is_not_fracture, test_is_not_fracture] = torch.utils.data.random_split(
        dataset_is_not_fracture, ((n_splits - 1) * fold_size, fold_size)
    )

    train = torch.utils.data.ConcatDataset([train_is_fracture, train_is_not_fracture])
    test = torch.utils.data.ConcatDataset([test_is_fracture, test_is_not_fracture])

    return [
        DataLoader(train, batch_size, shuffle=True),
        DataLoader(test, batch_size, shuffle=True),
    ]
